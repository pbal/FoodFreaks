﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
//using MySql.Data.MySqlClient;
using System.Text;

public partial class search : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        string con = ConfigurationManager.ConnectionStrings["OdbcSessionServices"].ToString();
        Response.Redirect("2.aspx?q=" + RemoveSpecialCharacters(Server.HtmlEncode(txtSearch.Text)).Replace(" ", "+"));
    }
    protected string RemoveSpecialCharacters(string str)
    {
        StringBuilder sb = new StringBuilder();
        foreach (char c in str)
        {
            if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || c == '.' || c == ' ')
            {
                sb.Append(c);
            }
        }
        return sb.ToString();
    }
}
