﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="search.ascx.cs" Inherits="search" %>

<script src="../scripts/jquery.autocomplete.js" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $("#<%=txtSearch.ClientID%>").autocomplete('Handler.ashx');
            $("#<%=txtSearch.ClientID%>").focus(function () {
                if ($(this).val() == 'Search for diners in Panchkula')
                    $(this).val('');
            });
            $("#<%=txtSearch.ClientID%>").blur(function () {
                if ($(this).val() == '')
                    $(this).val('Search for diners in Panchkula');
            });
    });      
</script>

<asp:TextBox Text="Search for diners in Panchkula" ID="txtSearch" runat="server"
    CssClass="grid_9 search"></asp:TextBox>
<asp:Button CssClass="grid_1 btnsearch" Text="Search!" runat="server" ID="btnSubmit"
    OnClick="btnSubmit_Click"></asp:Button>
