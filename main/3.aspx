﻿<%@ Page Language="C#" MasterPageFile="~/shared/MasterPage.master" AutoEventWireup="true"
    CodeFile="3.aspx.cs" Inherits="main_3" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="grid_9">
        <asp:Repeater ID="re" runat="server" OnItemDataBound="re_ItemDataBound">
            <ItemTemplate>
                <br />
                <h1 style="margin-top: 2px">
                    <asp:HyperLink ID="restlink" runat="server">
                        <asp:Literal ID="Name" runat="server" Text='<% #Eval("Name")%>'></asp:Literal>
                    </asp:HyperLink>
                </h1>
                </a>
                <div style="float: right; width: 149px; height: 15px; margin: 0">
                    <asp:Panel ID="ratpanel" runat="server">
                    </asp:Panel>
                </div>
                <br style="clear: both" />
                <p>
                    <asp:Literal ID="Phone" runat="server" Text='<% #Eval("Telephone")%>'></asp:Literal>
                    <br />
                    <asp:Literal ID="Literal1" runat="server" Text='<% #Eval("Address")%>'></asp:Literal>
                    <a href="#">[Find on Google Maps]</a></p>
                <p style="text-indent: 10px">
                    Cuisine :
                    <asp:Literal ID="Literal2" runat="server" Text='<% #Eval("Cuisine")%>'></asp:Literal></p>
                <p style="text-indent: 10px">
                    Time open :
                    <asp:Literal ID="Literal3" runat="server" Text='<% #Eval("TimeOpen")%>'></asp:Literal></p>
                <p style="text-indent: 10px">
                    Facilities :
                    <asp:Literal ID="Literal4" runat="server" Text='<% #Eval("Facilities")%>'></asp:Literal></p>
                <p style="text-indent: 10px">
                    Average cost for 2 : Rs
                    <asp:Literal ID="Literal5" runat="server" Text='<% #Eval("CostFor2")%>'></asp:Literal></p>
                <div class="sec menu">
                    <asp:TabContainer ID="TabContainer1" runat="server" Width="700">
                        <asp:TabPanel HeaderText="Menu" runat="server" ID="sa">
                            <HeaderTemplate>
                                <h2>
                                    Menu</h2>
                            </HeaderTemplate>
                            <ContentTemplate>
                                <p style="text-align: right;">
                                    Page 1 of 3</p>
                                <asp:ImageButton ImageUrl='<% #Eval("menu1")%>' ID="menupath" runat="server" ImageAlign="Middle"
                                    OnClick="menu" CommandName="menupath" />
                            </ContentTemplate>
                        </asp:TabPanel>
                        <asp:TabPanel HeaderText="Pictures" runat="server" ID="TabPanel2">
                            <HeaderTemplate>
                                <h2>
                                    Pictures</h2>
                            </HeaderTemplate>
                            <ContentTemplate>
                                <asp:ImageButton ImageUrl='<% #Eval("menu2")%>' ID="ImageButton2" runat="server"
                                    ImageAlign="Middle" OnClick="menu" CommandName="menupath" />
                            </ContentTemplate>
                        </asp:TabPanel>
                        <asp:TabPanel HeaderText="Maps" runat="server" ID="TabPanel1">
                            <HeaderTemplate>
                                <h2>
                                    Google Maps</h2>
                            </HeaderTemplate>
                            <ContentTemplate>
                                <iframe width="660" height="350" frameborder="0" scrolling="no" marginheight="0"
                                    marginwidth="0" src="http://maps.google.com/maps?hl=en&amp;ie=UTF8&amp;hq=&amp;hnear=Chandigarh,+India&amp;ll=30.700368,76.849247&amp;spn=0.048857,0.090895&amp;z=14&amp;output=embed">
                                </iframe>
                                <br />
                                <small><a href="http://maps.google.com/maps?hl=en&amp;ie=UTF8&amp;hq=&amp;hnear=Chandigarh,+India&amp;ll=30.700368,76.849247&amp;spn=0.048857,0.090895&amp;z=14&amp;source=embed"
                                    style="color: #0000FF; text-align: left">View Larger Map</a></small>
                            </ContentTemplate>
                        </asp:TabPanel>
                    </asp:TabContainer>
                </div>
            </ItemTemplate>
        </asp:Repeater>
        <div class="section nearby">
            <h2>
                Restaurants nearby</h2>
            <table>
                <asp:Repeater ID="nearby" runat="server" OnItemDataBound="nearby_ItemDataBound">
                    <ItemTemplate>
                        <tr>
                            <td>
                                <h3>
                                    <asp:HyperLink ID="restlink" runat="server">
                                        <asp:Literal ID="Literal7" runat="server" Text='<% #Eval("Name")%>'></asp:Literal></asp:HyperLink></h3>
                                <p>
                                    <a href="#" style="color: #469DBB; font-size: 12px">
                                        <asp:Literal ID="Cuisine" runat="server" Text='<% #Eval("Cuisine")%>'></asp:Literal></a>
                                </p>
                                <p>
                                    <asp:Literal ID="Address" runat="server" Text='<% #Eval("address")%>'></asp:Literal>
                                </p>
                            </td>
                    </ItemTemplate>
                    <AlternatingItemTemplate>
                        <td>
                            <h3>
                                <asp:HyperLink ID="restlink" runat="server">
                                    <asp:Literal ID="Literal8" runat="server" Text='<% #Eval("Name")%>'></asp:Literal></asp:HyperLink></h3>
                            <p>
                                <a href="#" style="color: #469DBB; font-size: 12px">
                                    <asp:Literal ID="Literal9" runat="server" Text='<% #Eval("Cuisine")%>'></asp:Literal></a>
                            </p>
                            <p>
                                <asp:Literal ID="Literal10" runat="server" Text='<% #Eval("address")%>'></asp:Literal>
                            </p>
                        </td>
                        </tr>
                    </AlternatingItemTemplate>
                </asp:Repeater>
            </table>
        </div>
        <div class="section userreviews ">
            <h2>
                User reviews for
                <asp:Literal ID="Literal6" runat="server" Text='<% #Eval("Name")%>'></asp:Literal></h2>
            <asp:Repeater ID="userReviews" runat="server">
                <ItemTemplate>
                    <h3>
                        <a href="#">
                            <asp:Literal ID="Literal7" runat="server" Text='<% #Eval("Name")%>'></asp:Literal></a></h3>
                    <p>
                        <a href="#" style="color: #469DBB; font-size: 12px">
                            <asp:Literal ID="Cuisine" runat="server" Text='<% #Eval("Cuisine")%>'></asp:Literal></a>
                    </p>
                    <p>
                        <asp:Literal ID="Address" runat="server" Text='<% #Eval("address")%>'></asp:Literal>
                    </p>
                </ItemTemplate>
            </asp:Repeater>
        </div>
        <div class="section review">
            <h2>
                Write your review
                <%--<asp:Literal ID="Literal6" runat="server" Text='<% #Eval("Name")%>'></asp:Literal>--%></h2>
            <p>
                If you already have an Foodie screenname, log in under the other tabs.
                <br />
                <br />
                Happy commenting, and keep it classy.</p>
            <table width="100%" id="review">
                <tr>
                    <td>
                        Rate this place:
                    </td>
                    <td>
                        <asp:Rating ID="Rating3" runat="server" MaxRating="5" StarCssClass="ratingStar" WaitingStarCssClass="savedRatingStar"
                            FilledStarCssClass="filledRatingStar" EmptyStarCssClass="emptyRatingStar">
                        </asp:Rating>
                    </td>
                </tr>
                <tr>
                    <td>
                        Name:
                    </td>
                    <td>
                        <asp:TextBox ID="txtName" CssClass="zn" runat="server" Width="300px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Email address:
                    </td>
                    <td>
                        <asp:TextBox ID="txtEmail" CssClass="zn" runat="server" Width="300px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        Write your review:<br />
                        <%--<span class="help-text" style="font-size: 10px">(minimum 50 characters)</span>--%>
                    </td>
                    <td>
                        <asp:TextBox ID="txtreview" CssClass="zn" TextMode="MultiLine" runat="server" Width="500px"
                            Height="200px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                    </td>
                    <td>
                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" />
                    </td>
                </tr>
            </table>
            <div class="clear">
            </div>
        </div>
    </div>
</asp:Content>
