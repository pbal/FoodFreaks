﻿<%@ Page Language="C#" MasterPageFile="~/shared/MasterPage.master" AutoEventWireup="true"
    CodeFile="2.aspx.cs" Inherits="main_2" %>

<%@ Register TagPrefix="cont" TagName="search" Src="~/controls/search.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script src="../Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="find">
        <select id="type" name="type" class="grid_2">
            <option value="everything">Everything</option>
        </select>
        <!--
				<input type="text" value="Search for radio stations" id="term" name="term" class="grid_4" />
				<p class="gutter">in</p>
				<input type="text" value="Any city, any state" id="location" name="location" class ="grid_2"/>
			-->
        <cont:search ID="searchbox" runat="server" />
        <div id="s-examples">
            <h3>
                Examples</h3>
            <ul>
                <li><a href="/search?type=everything&amp;term=KFLY">KFLY</a></li>
                <li><a href="/search?type=everything&amp;term=rock+oregon">rock oregon</a></li>
                <li><a href="/search?type=everything&amp;term=101.9">101.9</a></li>
                <li><a href="/search?type=everything&amp;term=portland">Portland</a></li>
            </ul>
        </div>
    </div>
    <h3>
        Restaurants in Panchkula</h3>
    <h4>
        <asp:Literal ID="ltlSearchReults" runat="server"></asp:Literal>
         
    </h4>
    <asp:DataGrid ID="DataGrid2" runat="server" CssClass="results grid_9 alpha" CellPadding="0"
        GridLines="None" AutoGenerateColumns="False" OnItemDataBound="DataGrid2_ItemDataBound"
        AllowPaging="True" ShowHeader="False" PagerStyle-Mode="NumericPages"
        PagerStyle-Position="TopAndBottom">
        <%--<EditItemStyle BackColor="#7C6F57" />
            <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />--%>
        <AlternatingItemStyle CssClass="alternateRusults" />
        <Columns>
            <asp:TemplateColumn>
                <ItemTemplate>
                    <h3>
                        <asp:HyperLink ID="restlink" runat="server">
                            <asp:Literal ID="Name" runat="server" Text='<% # DataBinder.Eval(Container.DataItem, "Name")%>'></asp:Literal></asp:HyperLink></h3>
                    <p>
                        <a href="#" style="color: #469DBB; font-size: 12px">
                            <asp:Literal ID="Cuisine" runat="server" Text='<% #DataBinder.Eval(Container.DataItem, "Cuisine")%>'></asp:Literal></a>
                    </p>
                    <p>
                        <asp:Literal ID="Address" runat="server" Text='<% #DataBinder.Eval(Container.DataItem, "address")%>'></asp:Literal>
                    </p>
                </ItemTemplate>
                <ItemStyle Width="350" />
            </asp:TemplateColumn>
        </Columns>
        <Columns>
            <asp:TemplateColumn>
                <ItemTemplate>
                    <%--<span style="font-size: 11px">
                            <asp:Literal ID="Rating" runat="server"></asp:Literal>
                        </span>--%>
                    <asp:Panel ID="ratpanel" runat="server">
                    </asp:Panel>
                </ItemTemplate>
                <ItemStyle Width="150" />
            </asp:TemplateColumn>
        </Columns>
        <Columns>
            <asp:TemplateColumn>
                <ItemTemplate>
                    <p style="text-align: right">
                        <a href="#">
                            <asp:Literal ID="Locality" runat="server" Text='<% #DataBinder.Eval(Container.DataItem, "Locality")%>'></asp:Literal></a>
                    </p>
                    <p style="text-align: right">
                        Cost for 2 Rs.
                        <asp:Literal ID="CostFor2" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Costfor2")%>'></asp:Literal>
                    </p>
                    <p class="facilities">
                    </p>
                </ItemTemplate>
                <ItemStyle Width="200" />
            </asp:TemplateColumn>
        </Columns>
        <PagerStyle CssClass="pagerresults" BackColor="#323033" ForeColor="White" />
    </asp:DataGrid>
    <br />
    <br />
    <br />
    <br />
    <%--<table class="results grid_9 alpha" border="0" cellspacing="0" cellpadding="0">
        <thead>
            <tr>
                <th scope="col" class="header headerSortDown">
                    <span class="tl"></span>Frequency
                </th>
                <th scope="col" class="header">
                    Callsign
                </th>
                <th scope="col" class="header">
                    Location
                </th>
                <th scope="col" class="header">
                    Tags
                </th>
                <th scope="col" class="header">
                    Favs
                </th>
                <td>
                </td>
            </tr>
        </thead>
        <tbody>
            <tr id="67801">
                <td>
                    <a href="/station/WNIN-FM">88.3 FM</a>
                </td>
                <td>
                    <a href="/station/WNIN-FM">WNIN-FM</a>
                </td>
                <td class="location">
                    <span class="overflow" title="Evansville, IN"><a href="/search?location=Evansville, IN">
                        Evansville, IN</a></span>
                </td>
                <td class="tag">
                    <span class="overflow" title="npr, classical music, pbs"><a href="/search?type=tags&amp;term=npr">
                        npr</a>, <a href="/search?type=tags&amp;term=classical+music">classical music</a>,
                        <a href="/search?type=tags&amp;term=pbs">pbs</a></span>
                </td>
                <td>
                    4
                </td>
                <td>
                    <a href="http://durocast.com/player/player.php?facility_id=67801" class="play" title="88.3 FM WNIN-FM">
                        Play</a> <a href="#" class="fav off">Fav</a><a class="popup1" href="http://www.wnin.org">w</a>
                </td>
            </tr>
            <tr id="35501">
                <td>
                    <a href="/station/KQED-FM">88.5 FM</a>
                </td>
                <td>
                    <a href="/station/KQED-FM">KQED-FM</a>
                </td>
                <td class="location">
                    <span class="overflow" title="San Francisco, CA"><a href="/search?location=San Francisco, CA">
                        San Francisco, CA</a></span>
                </td>
                <td class="tag">
                    <span class="overflow" title="public radio, public radio npr, science friday"><a
                        href="/search?type=tags&amp;term=public+radio">public radio</a>, <a href="/search?type=tags&amp;term=public+radio+npr">
                            <b>public radio npr</b></a>, <a href="/search?type=tags&amp;term=science+friday">science
                                friday</a></span>
                </td>
                <td>
                    6
                </td>
                <td>
                    <a href="http://durocast.com/player/player.php?facility_id=35501" class="play" title="88.5 FM KQED-FM">
                        Play</a> <a href="#" class="fav off">Fav</a><a class="popup1" href="http://www.kqed.org">w</a>
                </td>
            </tr>
        </tbody>
    </table>--%>
    <%--<asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:OdbcSessionServices %>"
        EnableCaching="true" DataSourceMode="DataSet" ProviderName="<%$ ConnectionStrings:OdbcSessionServices.ProviderName %>"
        SelectCommand="SELECT * FROM `pklrestaurants`"></asp:SqlDataSource>--%>
</asp:Content>
