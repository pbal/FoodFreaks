﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
//using MySql.Data.MySqlClient;

public partial class main_2 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string param = string.Empty;
        if (Request.QueryString["q"] != null)
        {
            param = Request.QueryString["q"].ToString();
            ltlSearchReults.Text = "Search results for :" + Request.QueryString["q"].ToString();
        }
        else
        {
            param = "all";
            ltlSearchReults.Text = "Showing all restraunts from Panchkula";
        }
        string con = ConfigurationManager.ConnectionStrings["OdbcSessionServices"].ToString();

        DataSet ds = new DataSet();
        //using (MySqlConnection conn = new MySqlConnection(con))
        //{
        //    MySqlDataAdapter da = new MySqlDataAdapter("rtFetchResults", conn);
        //    da.SelectCommand.CommandType = CommandType.StoredProcedure;
        //    da.SelectCommand.Parameters.AddWithValue("value", param);
        //    da.Fill(ds);
        //}
        DataGrid2.DataSource = ds;
        DataGrid2.DataBind();
        DataGrid2.PageSize = 6;
    }

    protected void DataGrid2_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Header || e.Item.ItemType == ListItemType.Footer) return;
        DataRowView rec = (DataRowView)e.Item.DataItem;
        ((HyperLink)e.Item.FindControl("restlink")).NavigateUrl = "3.aspx?q=" + rec.Row.ItemArray[25].ToString();

        int rating = (int)(Convert.ToSingle(rec.Row.ItemArray[5].ToString()) * 2);
        Panel a = (Panel)e.Item.FindControl("ratpanel");

        //Image p = new Image();
        //p.ImageUrl = "~/images/ratingsprite.gif";
        #region rating 
        switch (rating)
        {
            case 0:
                a.CssClass = "star0";
                break;
            case 1:
                a.CssClass = "star1";
                break;
            case 2:
                a.CssClass = "star2";
                break;
            case 3:
                a.CssClass = "star3";
                break;
            case 4:
                a.CssClass = "star4";
                break;
            case 5:
                a.CssClass = "star5";
                break;
            case 6:
                a.CssClass = "star6";
                break;
            case 7:
                a.CssClass = "star7";
                break;
            case 8:
                a.CssClass = "star8";
                break;
            case 9:
                a.CssClass = "star9";
                break;
            case 10:
                a.CssClass = "star10";
                break;
            default:
                break;
        }
        #endregion
    }
}
