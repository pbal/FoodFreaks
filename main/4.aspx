﻿<%@ Page Language="C#" MasterPageFile="~/shared/MasterPage.master" AutoEventWireup="true"
    CodeFile="4.aspx.cs" Inherits="main_1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="../Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>

    <script type="text/javascript" src="../scripts/jquery.cycle.all.min.js"></script>

    <script src="../scripts/jquery.autocomplete.js" type="text/javascript"></script>

    <script type="text/javascript">
    $(document).ready(function() {
        $("#<%=term.ClientID%>").autocomplete('Handler.ashx');
    });      
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:TextBox ID="term" runat="server"  ></asp:TextBox>
     
    <%--<div id="find">
         
         <asp:TextBox Text="Search for diners in Panchkula" ID="term" runat="server" CssClass="grid_9 search"></asp:TextBox>
        
        <input type="submit" class="grid_1" value="Search!" />
        
        <div id="s-examples">
            <h3>
                Examples</h3>
            <ul>
                <li><a href="/search?type=everything&amp;term=KFLY">KFLY</a></li>
                <li><a href="/search?type=everything&amp;term=rock+oregon">rock oregon</a></li>
                <li><a href="/search?type=everything&amp;term=101.9">101.9</a></li>
                <li><a href="/search?type=everything&amp;term=portland">Portland</a></li>
            </ul>
        </div>
        <p class="example everything grid_7 prefix_3 alpha omega">
            Examples: <a href="/search?type=everything&amp;term=KFLY">KFLY</a>, <a href="/search?type=everything&amp;term=rock+oregon">
                rock oregon</a>, <a href="/search?type=everything&amp;term=101.9">101.9</a>,
            <a href="/search?type=everything&amp;term=portland">Portland</a></p>
        <p class="example callsign grid_7 prefix_5 alpha omega">
            Examples: <a href="/search?type=callsign&amp;term=KFLY">KFLY</a>, <a href="/search?type=callsign&amp;term=KINK">
                KINK</a>, <a href="/search?type=callsign&amp;term=KFUO">KUFO</a></p>
        <p class="example frequency grid_7 prefix_5 alpha omega">
            Examples: <a href="/search?type=frequency&amp;term=101.1">101.1</a>, <a href="/search?type=frequency&amp;term=101.9">
                101.9</a>, <a href="/search?type=frequency&amp;term=101.5">101.5</a></p>
        <p class="example tags grid_7 prefix_5 alpha omega">
            Examples: <a href="/search?type=tags&amp;term=blues">Blues</a>, <a href="/search?type=tags&amp;term=rock">
                Rock</a>, <a href="/search?type=tags&amp;term=talk show">Talk show</a></p>
        <p class="example users grid_7 prefix_5 alpha omega">
            Examples: <a href="#">elliothere</a>, <a href="#">BryceClems</a>, <a href="#">Matt</a></p>
    </div>
    <h2>
        Discover the best places to eat in Panchkula</h2>
    <div id="boxes" class="grid_12">
        <div class="box explore grid_4 alpha">
            <h3 class="shadow">
                Featured Reviews:</h3>
            <p>
                What's the DuroCast team listening to? Staff picks:</p>
            <p>
                <a href="station/KINK">KINK</a>, <a href="station/KUFO">KUFO</a>, <a href="station/KRSK">
                    KRSK</a></p>
            <p>
                Keep track of the DuroCast team:</p>
            <p>
                <a href="person/elliothere">Elliot Swan</a>, <a href="person/bumboarder6">Matt Polzin</a>,
                and <a href="person/Bryce">Bryce Clemmer</a></p>
        </div>
        <div class="box omega" style="width: 600px; height: 325px; overflow: hidden;">
            <div id="coin-slider">
                <a href="?p=28">
                    <img src="../images/bluestraw600.gif" class="attachment-slider-post-thumbnail wp-post-image"
                        alt="gallery-1" title="gallery-1" /></a> <a href="?p=28">
                            <img src="../images/rasb600.jpg" class="attachment-slider-post-thumbnail wp-post-image"
                                alt="gallery-1" title="gallery-1" /></a> <a href="?p=28">
                                    <img src="../images/strawfork600.gif" class="attachment-slider-post-thumbnail wp-post-image"
                                        alt="gallery-1" title="gallery-1" /></a>
            </div>
            <div id="controllers" class="clearfix">
            </div>
        </div>
        
    </div>
    <div style="clear: both">
    </div>
    <div class="push">
    </div>
    <div id="browse" class="grid_12">
        <ul>
            <li class="browsehe"><a href="#"><b>Most Viewed Restaraunts</b></a></li>
            <li class="browsehe"><a href="#"><b>Cuisine</b></a></li>
            <li class="browsehe"><a href="#"><b>Best For</b></a> </li>
            <li class="browsehe"><a href="#"><b>Special Offers</b></a></li>
            <li><a href="#">Kava</a></li>
            <li><a href="#">North Indian</a></li>
            <li><a href="#">Good Food</a></li>
            <li><a href="#">Below Rs500</a></li>
            <li><a href="#">Copper Chimney</a></li>
            <li><a href="#">Chineese</a></li>
            <li><a href="#">Hot Venues</a></li>
            <li><a href="#">30% Off</a></li>
            <li><a href="#">Lobby</a></li>
            <li><a href="#">Continental</a></li>
            <li><a href="#">Kid Friendly</a></li>
            <li><a href="#">2 For 1</a></li>
            <li><a href="#">Page 3</a></li>
            <li><a href="#">Bars</a> </li>
            <li><a href="#">Outdoor</a></li>
            <li><a href="#">Dinner and Theatre</a></li>
            <li><a href="#">Western Court</a></li>
            <li><a href="#">Thai</a></li>
            <li><a href="#">Quick Bite</a></li>
            <li><a href="#">Lunch Deals</a></li>
        </ul>
    </div>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:OdbcSessionServices %>"
        EnableCaching="true" DataSourceMode="DataSet" ProviderName="<%$ ConnectionStrings:OdbcSessionServices.ProviderName %>"
        SelectCommand="SELECT * FROM `pklrestaurants`"></asp:SqlDataSource>
     <script type="text/javascript">

        /* Using multiple unit types within one animation. */
        $(document).ready(function () {
            $("#ctl00_ContentPlaceHolder1_term").focus(function () {
                if ($(this).val() == 'Search for diners in Panchkula')
                    $(this).val('');
            });
            $("#ctl00_ContentPlaceHolder1_term").blur(function () {
                if ($(this).val() == '')
                    $(this).val('Search for diners in Panchkula');
            });
             
			$('#coin-slider').cycle({
				timeout: 4500,			
				speed: 500,
				cleartypeNoBg: true,
				cleartype: true, 
				pager: 'div#controllers',
				fx: 'fade',
				pause: 1});
	 
        });
    </script>

--%></asp:Content>
